"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.conf import settings
from django.conf.urls.static import static

import fitur_loginStatus.urls as loginStatus
import fitur_profile.urls as profile
import fitur_riwayat.urls as riwayat
import fitur_cariMahasiswa.urls as cariMahasiswa


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^fitur-login-status/', include(loginStatus,namespace='fitur_loginStatus')),
    url(r'^fitur-profile/', include(profile,namespace='fitur-profile')),
    url(r'^fitur-riwayat/', include(riwayat,namespace='fitur-riwayat')),
    url(r'^fitur-cariMahasiswa/', include(cariMahasiswa,namespace='fitur-cariMahasiswa')),
	url(r'^$', RedirectView.as_view(url="fitur-login-status/", permanent="true"), name='index'),
]
