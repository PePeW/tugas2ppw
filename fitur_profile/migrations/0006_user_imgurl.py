# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-13 02:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fitur_profile', '0005_auto_20171212_1856'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='imgUrl',
            field=models.CharField(default='-', max_length=400),
        ),
    ]
