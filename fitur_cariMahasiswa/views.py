from django.shortcuts import render 
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from .models import Mahasiswa
from fitur_profile.models import User
from fitur_profile.views import response

# Create your views here.
def index(request):
	if 'user_login' not in request.session.keys():
		return HttpResponseRedirect(reverse('fitur_loginStatus:index'))

	list_login = User.objects.all()
	html = 'fitur_cariMahasiswa/fitur_cariMahasiswa.html'
	response.update({"mahasiswa_list" : list_login})
	return render(request, html, response)

def dummy(request):
	mahasiswa_list = Mahasiswa.objects.all().values()
	html = 'fitur_cariMahasiswa/fitur_cariMahasiswa.html'
	response.update({"mahasiswa_list" : mahasiswa_list})
	return render(request, html, response)
	