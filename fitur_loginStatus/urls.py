from django.conf.urls import url
from .views import index, displayStatus, add_post
# sol to challenge
# from .views import add_session_item, del_session_item, clear_session_item
# /sol
from Package.custom_auth import auth_login, auth_logout


urlpatterns = [
    url(r'^$', index, name='index'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
    url(r'^status/$', displayStatus, name='displayStatus'),
    url(r'^add-post/$', add_post, name='add-post'),



]
