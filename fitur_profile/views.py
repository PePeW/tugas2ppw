from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from .models import User
from django.views.decorators.csrf import csrf_exempt
import json
from fitur_loginStatus.models import *
from fitur_loginStatus.forms import *

# Create your views here.
response = {}
def index(request):

    ## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('fitur_loginStatus:index'))
    ## end of sol
    set_data_for_session(response, request)

    sudahAda = User.objects.filter(npm=response['kode_identitas'])
    if not sudahAda :
        newUser = User.objects.create(npm=response['kode_identitas'])
        newUser.save()
        print("User Baru"+str(newUser.npm))

    pengguna = User.objects.get(npm=response['kode_identitas'])
    response['id'] = pengguna.id

    get_data_user(pengguna)

    postt = Post.objects.order_by('-created_date')

    count = 0;
    count = Post.objects.all().count()
    response['total'] = count
    print("dasdadadasda")

    response['post'] = postt

    status = "kosong"
    status = Post.objects.all()[count-1].content
    response['statusku'] = status
    print("abc")

    html = 'fitur_profile/profilePage.html'
    return render(request, html, response)

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def editProfile(request):
    html = 'fitur_profile/editProfile.html'
    return render(request, html, response)

def get_data_user(pengguna):
    response['user'] = pengguna
    response['keahlian'] = pengguna.skill.split(" ")

@csrf_exempt
def change_user_data(request):
    if request.method == 'POST':
        pengguna = User.objects.get(npm=request.session['kode_identitas'])
        pengguna.name = request.POST.get('name')
        pengguna.skill = request.POST.get('skills')
        pengguna.email = request.POST.get('email')
        pengguna.linkProfile = request.POST.get('linkin')
        pengguna.imgUrl = request.POST.get('url')
        pengguna.save()
        print(pengguna.name)
    return HttpResponseRedirect(reverse('fitur-profile:index'))

def get_allSkill(request):
    pengguna = User.objects.get(npm=request.session['kode_identitas'])
    lst = pengguna.skill.strip().split(" ")
    return JsonResponse({'ahli':lst})
