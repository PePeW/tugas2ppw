# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import Post_Form
from .models import Post
from fitur_profile.views import response

def index(request):
    response['author'] = 'PePeW'
    print ("#==> masuk index")
    if 'user_login' in request.session:

        response['login_status'] = True
        return HttpResponseRedirect(reverse('fitur-profile:index'))
    else:

        html = 'fitur_loginStatus/session/login.html'
        response['login_status'] = False
        return render(request, html, response)

def displayStatus(request):
    html = 'status_page.html'
    response['Post_Form'] = Post_Form
    return render(request, html, response)

def add_post(request):
	form = Post_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['post'] = request.POST['post']
		post = Post(content=response['post'])
		post.save()
	return HttpResponseRedirect('/fitur-login-status/status/')




### General Function
# def add_session_item(request, key, id):
#     print ("#ADD session item")
#     ssn_key = request.session.keys()
#     if not key in ssn_key:
#         request.session[key] = [id]
#     else:
#         items = request.session[key]
#         if id not in items:
#             items.append(id)
#             request.session[key] = items
#
#     msg = "Berhasil tambah " + key +" favorite"
#     messages.success(request, msg)
#     return HttpResponseRedirect(reverse('lab-9:profile'))
#
# def del_session_item(request, key, id):
#     print ("# DEL session item")
#     items = request.session[key]
#     print ("before = ", items)
#     items.remove(id)
#     request.session[key] = items
#     print ("after = ", items)
#
#     msg = "Berhasil hapus item " + key + " dari favorite"
#     messages.error(request, msg)
#     return HttpResponseRedirect(reverse('lab-9:profile'))
#
# def clear_session_item(request, key):
#     del request.session[key]
#     msg = "Berhasil hapus session : favorite " + key
#     messages.error(request, msg)
#     return HttpResponseRedirect(reverse('lab-9:index'))

# ======================================================================== #
