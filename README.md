
Nama Kelompok :
- Donny Samuel
- Faraya Agatha P.
- Jeremy Valentino
- Lowell H. S.

Status Pipeline :
[![pipeline status](https://gitlab.com/PePeW/tugas2ppw/badges/master/pipeline.svg)](https://gitlab.com/PePeW/tugas2ppw/commits/master)

status code coverage:
[![coverage report](https://gitlab.com/PePeW/tugas2ppw/badges/master/coverage.svg)](https://gitlab.com/PePeW/tugas2ppw/commits/master)

link heroku:
https://tugas2ppw.herokuapp.com/
