from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import User
from Package.csui_helper import *
# Create your tests here.

class profileUnitTest(TestCase):
    # def test_url_is_exist(self):
    #     response = Client().get('/fitur-profile/')
    #     self.assertEqual(response.status_code,302)
    #     session = self.client.session
    #     session['user_login'] = "oke"
    #     session['access_token'] = "oke"
    #     session['kode_identitas'] = "1606881001"
    #     session['role'] = "oke"
    #     session.save()
    #     response = self.client.get('/fitur-profile/')
    #     self.assertEqual(response.status_code,200)

    def test_edit_profile(self):
        response = Client().get('/fitur-profile/profile/edit/')
        self.assertEqual(response.status_code,200)

    def test_save_profile(self):
        response = Client().get('/fitur-profile/profile/save/')
        self.assertEqual(response.status_code,302)

    def test_get_data(self):
        newUser = User.objects.create(npm="123")
        newUser.save()
        session = self.client.session
        session['kode_identitas'] = "123"
        session.save()
        response = self.client.get('/fitur-profile/get_data/')
        self.assertEqual(type(response.json()), dict)

    def test_change_user_date(self):
        newUser = User.objects.create(npm="1234")
        newUser.save()
        session = self.client.session
        session['kode_identitas'] = "1234"
        session.save()
        response = self.client.post('/fitur-profile/profile/save/',{"name":"", "skills":"", "email":"", "linkin":"", "url":""})
        self.assertEqual(response.status_code, 302)
