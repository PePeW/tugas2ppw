from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile/edit/$', editProfile, name='edit'),
    url(r'^profile/save/$', change_user_data, name='save'),
    url(r'^get_data/$', get_allSkill, name="skill"),
]
