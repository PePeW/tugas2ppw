from django.db import models

# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=200, default="-", null=True)
    npm = models.CharField(max_length=200, default="-", null=True)
    skill = models.CharField(max_length=200, default="-", null=True)
    email = models.CharField(max_length=200, default="-", null=True)
    linkProfile = models.CharField(max_length=200, default="-", null=True)
    imgUrl = models.CharField(max_length=400, default="-", null=True)
