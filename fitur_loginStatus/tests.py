from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Post
from Package.csui_helper import *
# Create your tests here.

class loginUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/fitur-login-status/')
        self.assertEqual(response.status_code,200)
        session = self.client.session
        session['user_login'] = "oke"
        session.save()
        response = self.client.get('/fitur-login-status/')
        self.assertEqual(response.status_code,302)

    def test_display_status(self):
        response = Client().get('/fitur-login-status/status/')
        self.assertEqual(response.status_code,200)

    def test_add_post(self):
        name = "aku"
        email = "a@hgmail.com"
        message = "oke"
        response = Client().post('/fitur-login-status/add-post/', {'name': name, 'email': email, 'message': message})
        self.assertEqual(response.status_code, 302)
